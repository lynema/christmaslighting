#!/usr/bin/env python3
import numpy as np, sounddevice as sd, sys, time, threading, gpiozero

pins = [26, 13, 6, 5]
relays = {pin : gpiozero.OutputDevice(pin, active_high=True, initial_value=False) for pin in pins}
lock = threading.Lock()

print (relays)
print (sd.query_devices())
last_toggle_time = float(0)
toggle_time_delta = .5
level_1_threshhold = 2
level_2_threshhold = 5
level_3_threshhold = 9
level_4_threshhold = 14

def turn_on_relay(relay):
    if relay.value == 0: relay.toggle()
    #if relay.value == 1: relay.off()

def turn_off_relay(relay):
    if relay.value == 1: relay.toggle()
    #if relay.value == 0: relay.on()



def turn_off_highest_relay():
    #pass
    for pin in pins:
        if relays.get(pin).value == 1:
            turn_off_relay(relays.get(pin))
            break;

def audio_callback(indata, frames, time, status):
  global last_toggle_time
  volume_norm = np.linalg.norm(indata) * 10
  if "debug" in sys.argv: print("|" * int(volume_norm) + str(volume_norm) + str(time.currentTime) + "last" + str(last_toggle_time))
  mark_for_turn_off = False
  if toggle_time_delta < time.currentTime - last_toggle_time:
    lock.acquire()
    last_toggle_time = time.currentTime
    if volume_norm > level_4_threshhold:
      if "debug" in sys.argv: print ("L4")
      turn_on_relay(relays.get(pins[3]))
      turn_on_relay(relays.get(pins[2]))
      turn_on_relay(relays.get(pins[1]))
      turn_on_relay(relays.get(pins[0]))
    elif volume_norm > level_3_threshhold:
      if "debug" in sys.argv: print ("L3")
      turn_on_relay(relays.get(pins[3]))
      turn_on_relay(relays.get(pins[2]))
      turn_on_relay(relays.get(pins[1]))
    elif volume_norm > level_2_threshhold:
      if "debug" in sys.argv: print ("L2")
      turn_on_relay(relays.get(pins[3]))
      turn_on_relay(relays.get(pins[2]))
    elif volume_norm > level_1_threshhold:
      if "debug" in sys.argv: print ("L1")
      turn_on_relay(relays.get(pins[3]))
    else:  
      mark_for_turn_off = True
      if "debug" in sys.argv: print ("L0")
    if mark_for_turn_off:
        turn_off_highest_relay()
    lock.release()
stream = sd.InputStream(blocksize=1024, callback=audio_callback)
with stream:
    try:
        while True:
          sd.sleep(1000*10)
    except KeyboardInterrupt:
        print("\nExiting application\n")
        # exit the application
        sys.exit(0)

