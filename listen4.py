#!/usr/bin/env python3
import numpy as np, sounddevice as sd, sys, time, threading, gpiozero

pins = (5, 6, 13, 26)
thresholds = (2, 5, 9, 14)

relays = [gpiozero.OutputDevice(pin, active_high=True, initial_value=False) for pin in pins]
lock = threading.Lock()
toggle_time_delta = 1

print (sd.query_devices())
last_toggle_time = float(0)
highest_active_relay = -1

def turn_on_relay(relay):
    if not relay.is_active: relay.toggle()

def turn_off_highest_relay():
    global highest_active_relay
    for relay in reversed(relays):
        if relay.is_active:
            relay.toggle()
            highest_active_relay -= 1
            break

def get_level_from_volume(volume):
    if volume < thresholds[0]: return -1
    level = 0
    for threshold in thresholds[1:]:
        if threshold < volume:
            level += 1
    return level

def audio_callback(indata, frames, time, status):
  global last_toggle_time, highest_active_relay
  volume_norm = np.linalg.norm(indata) * 10
  level = get_level_from_volume(volume_norm) 
  if "debug" in sys.argv: print("|" * int(volume_norm) + str(volume_norm) + str(time.currentTime) + "last" + str(last_toggle_time) + " level" + str(level))
  lock.acquire()
  if (toggle_time_delta < time.currentTime - last_toggle_time) or (highest_active_relay < level):
    last_toggle_time = time.currentTime
    if level >= 0:
      highest_active_relay = level
      for relay in relays[0:level+1]: turn_on_relay(relay) 
    else:  
      turn_off_highest_relay()
  lock.release()

stream = sd.InputStream(blocksize=1024, callback=audio_callback)
with stream:
    try:
        while True:
          sd.sleep(1000*10)
    except KeyboardInterrupt:
        print("\nExiting application\n")
        sys.exit(0)
