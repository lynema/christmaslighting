#!/usr/bin/env python3
import numpy as np, sounddevice as sd, sys, time, threading, gpiozero

pins = [26, 13, 6, 5]
relays = {pin : gpiozero.OutputDevice(pin, active_high=False, initial_value=False) for pin in pins}
lock = threading.Lock()

print (relays)
print (sd.query_devices())
last_toggle_time = float(0)
toggle_time_delta = .5
level_1_threshhold = 2
level_2_threshhold = 4
level_3_threshhold = 8
level_4_threshhold = 12

def turn_on_relay(relay):
    if relay.value == 0: relay.on()

def turn_off_relay(relay):
    if relay.value == 1: relay.off()


def turn_off_highest_relay():
    for pin in pins:
        if relays.get(pin).value == 1:
            turn_off_relay(relays.get(pin))
            break;

def audio_callback(indata, frames, time, status):
  global last_toggle_time
  volume_norm = np.linalg.norm(indata) * 10
  print("|" * int(volume_norm) + str(volume_norm) + str(time.currentTime))
  lock.acquire()
  mark_for_turn_off = False
  if time.currentTime - last_toggle_time > toggle_time_delta:
    last_toggle_time = time.currentTime
    if volume_norm > level_4_threshhold:
      turn_on_relay(relays.get(pins[3]))
      turn_on_relay(relays.get(pins[2]))
      turn_on_relay(relays.get(pins[1]))
      turn_on_relay(relays.get(pins[0]))
    elif volume_norm > level_3_threshhold:
      turn_on_relay(relays.get(pins[3]))
      turn_on_relay(relays.get(pins[2]))
      turn_on_relay(relays.get(pins[1]))
      turn_off_relay(relays.get(pins[0]))
    elif volume_norm > level_2_threshhold:
      turn_on_relay(relays.get(pins[3]))
      turn_on_relay(relays.get(pins[2]))
      turn_off_relay(relays.get(pins[1]))
      turn_off_relay(relays.get(pins[0]))
    elif volume_norm > level_1_threshhold:
      turn_on_relay(relays.get(pins[3]))
      mark_for_turn_off = True
    else:  mark_for_turn_off = True
    if mark_for_turn_off:
        turn_off_highest_relay()
  lock.release()
stream = sd.InputStream(blocksize=1024, callback=audio_callback)
with stream:
    try:
        while True:
          sd.sleep(1000*10)
    except KeyboardInterrupt:
        print("\nExiting application\n")
        # exit the application
        sys.exit(0)

