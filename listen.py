#!/usr/bin/env python3
import numpy as np
import sounddevice as sd
#sd.default.device = None, 1
import sys
import time
import threading

import gpiozero

# change this value based on which GPIO port the relay is connected to
RELAY_PIN_1 = 26
RELAY_PIN_2 = 13
RELAY_PIN_3 = 6
RELAY_PIN_4 = 5

lock = threading.Lock()

# create a relay object.
# Triggered by the output pin going low: active_high=False.
# Initially off: initial_value=False
relay1 = gpiozero.OutputDevice(RELAY_PIN_1, active_high=False, initial_value=False)
relay2 = gpiozero.OutputDevice(RELAY_PIN_2, active_high=False, initial_value=False)
relay3 = gpiozero.OutputDevice(RELAY_PIN_3, active_high=False, initial_value=False)
relay4 = gpiozero.OutputDevice(RELAY_PIN_4, active_high=False, initial_value=False)


print (sd.query_devices())
duration = 15 #in seconds
current_level = 0
last_toggle_time = 0
toggle_time_delta = .5
level_1_threshhold = 2
level_2_threshhold = 30
level_3_threshhold = 80
level_4_threshhold = 125


def turn_off_highest_relay():
    if relay4.value == 1:
      relay4.off()
    elif relay3.value == 1:
      relay3.off()
    elif relay2.value == 1:
      relay2.off()
    elif relay1.value ==1:
      relay1.off()

def turn_on_relay(relay):
    if relay.value == 0:
      relay.on()

def turn_off_relay(relay):
    if relay.value == 1:
      relay.off()

def audio_callback(indata, frames, time, status):
  global last_toggle_time
  volume_norm = np.linalg.norm(indata) * 10
  print("|" * int(volume_norm) + str(volume_norm))
  print(time.currentTime)
  if time.currentTime - last_toggle_time > toggle_time_delta:
    last_toggle_time = time.currentTime
    if volume_norm > level_1_threshhold:
      lock.acquire()
      turn_on_relay(relay1)
      if volume_norm > level_2_threshhold:
        turn_on_relay(relay2)
        if volume_norm > level_3_threshhold:
          turn_on_relay(relay3)
          if volume_norm > level_4_threshhold:
            turn_on_relay(relay4)
      lock.release()
    else:
      turn_off_highest_relay()
stream = sd.InputStream(callback=audio_callback)
with stream:
    try:
        while True:
          sd.sleep(1000*10)
    except KeyboardInterrupt:
        # turn the relay off
        turn_off_relay(relay1)
        turn_off_relay(relay2)
        turn_off_relay(relay3)
        turn_off_relay(relay4)
        print("\nExiting application\n")
        # exit the application
        sys.exit(0)

