#!/usr/bin/env python3
#sd.default.device = None, 1
import sys
import time
import random
import gpiozero

# change this value based on which GPIO port the relay is connected to
RELAY_PIN_1 = 26
RELAY_PIN_2 = 13
RELAY_PIN_3 = 6
RELAY_PIN_4 = 5

# create a relay object.
# Triggered by the output pin going low: active_high=False.
# Initially off: initial_value=False
relay1 = gpiozero.OutputDevice(RELAY_PIN_1, active_high=False, initial_value=False)
relay2 = gpiozero.OutputDevice(RELAY_PIN_2, active_high=False, initial_value=False)
relay3 = gpiozero.OutputDevice(RELAY_PIN_3, active_high=False, initial_value=False)
relay4 = gpiozero.OutputDevice(RELAY_PIN_4, active_high=False, initial_value=False)

def turn_off_highest_relay():
    if relay4.value == 1:
      relay4.off()
    elif relay3.value == 1:
      relay3.off()
    elif relay2.value == 1:
      relay2.off()
    elif relay1.value ==1:
      relay1.off()

def toggle_relay(relay):
      relay.toggle()

def main_loop():
    while True:
      time.sleep(1.75)
      toggle_list = random.sample(range(1,5), 3)
      for i in toggle_list:
        if i == 1:
            toggle_relay(relay1)
        if i == 2:
            toggle_relay(relay2)
        if i == 3:
            toggle_relay(relay3)
        if i == 4:
            toggle_relay(relay4)
  
if __name__ == "__main__":
    try:
        main_loop()
    except KeyboardInterrupt:
        # turn the relay off
        print("\nExiting application\n")
        # exit the application
        sys.exit(0)
