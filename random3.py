#!/usr/bin/env python3
import time, sys, random, gpiozero

pins = (26, 13, 6, 5)
relays = {pin : gpiozero.OutputDevice(pin, active_high=False, initial_value=False) for pin in pins}

while True: 
    time.sleep(random.randrange(3))
    for key in random.sample(relays.keys(), 2): relays.get(key).toggle() 
